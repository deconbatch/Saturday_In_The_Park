/**
 * Saturday In The Park.
 * A random walk on the grid.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.02.15
 */

void setup() {
  size(720, 480);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();
  noLoop();
}

void draw() {
  int frmMax  = 24 * 3; // 24fps * 3s animation with each pattern
  int corners = 3;      // initial pattern = 3 corners shape
  
  translate(width * 0.5, height * 0.5);

  for (int pattern = 0; pattern < 3; pattern++) {

    corners += pattern; // 3, 4, 6
    int   walkerMax = 21 - pattern * 4;
    int   tailMax   = corners * 3; // walker's tail
    float sideLen   = 8.0 - pattern * 2.0;

    ArrayList walkers = new ArrayList();
    for (int i = 0; i < walkerMax; i++) {
      float wX = sideLen * corners * i - (sideLen * corners * walkerMax) * 0.5;
      float wY = 0.0;
      float wC = (50.0 * i) % 360.0;
      float wD = TWO_PI / corners;
      int   wR = floor(random(corners));
      ArrayList<Walker> tails = new ArrayList<Walker>();
      for (int j = 0; j < tailMax; j++) {
        tails.add(new Walker(wX, wY, wC, wD, wR));
      }
      walkers.add(tails);
    }

    for(int frmCnt = 0; frmCnt < frmMax; frmCnt++) {
      background(0.0, 0.0, 100.0, 100.0);
      strokeWeight(5.0);
      noFill();

      for (int i = 0; i < walkers.size(); i++) {
        float wX = 0.0;
        float wY = 0.0;
        float wC = 0.0;
        float wD = 0.0;
        int   wR = 0;
        
        ArrayList<Walker> tails = (ArrayList)walkers.get(i);
        for (Walker walker : tails) {
          wX = walker.x;
          wY = walker.y;
          wC = walker.colour;
          wD = walker.radianDiv;
          wR = walker.rotateCnt;

          stroke(wC, 40.0, 80.0, 100.0);
          beginShape();
          vertex(wX, wY);
          for (int j = 0; j < corners; j++) {
            // it makes a straight line between corners
            wX += sideLen * cos(wR * wD);
            wY += sideLen * sin(wR * wD);
            vertex(wX, wY);
          }
          endShape();
        }
        
        if (abs(wX) < width * 0.35 && abs(wY) < height * 0.35) {
          if (random(1.0) < 0.5) {
            --wR; // not turn
          } else {
            if (random(1.0) < 0.1) {
              wD *= -1.0; // turn
            }
          }
        }
        
        ++wR;
        wR %= corners;
        // tail shift
        tails.add(new Walker(wX, wY, wC, wD, wR));
        tails.remove(0);
      }

      casing();

      // for stop motion of first pattern
      if (frmCnt == 0) {
        for (int i = 0; i < 18; i++) {
          saveFrame("frames/" + String.format("%02d", corners) + ".00." + String.format("%04d", i) + ".png");
        }
      }

      saveFrame("frames/" + String.format("%02d", corners) + ".01." + String.format("%04d", frmCnt) + ".png");
    }
  }
  exit();

}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(20.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(15.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}

/**
 * Walker : hold random walker attributes
 */
private class Walker {
  public float x, y;
  public float colour;
  public float radianDiv;
  public int   rotateCnt;

  Walker(float _x, float _y, float _c, float _d, int _r) {
    x = _x;
    y = _y;
    colour = _c;
    radianDiv = _d;
    rotateCnt = _r;
  }
}
